package com.userfrom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VitasoftFormApplication {

	public static void main(String[] args) {
		SpringApplication.run(VitasoftFormApplication.class, args);
	}

}
