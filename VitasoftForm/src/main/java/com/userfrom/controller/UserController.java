package com.userfrom.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.userfrom.model.User;
import com.userfrom.service.UserService;

@RestController
public class UserController {
	@Autowired
	private UserService userService;
	
	@PostMapping("/form/save-details")
	public User create(@RequestBody User user) {
		return userService.save(user);
		
	}
	
	@GetMapping("/form/get-details/{id}")
	public User getUserDetails(@PathVariable("id")int id) {
		return userService.getUserDetails(id);
	}
	
	
    @DeleteMapping("/form/delete-details/{id}")
	public User deleteUserDetails(@PathVariable("id")int id) {
		return userService.deleteUserDetails(id);
	}
    
    @PutMapping("/form/update-details/{id}")
    public User updateUserDetails(@RequestBody User user,@PathVariable("id")int id) {
    	User myuser =userService.getUserDetails(id);
    		if (myuser != null) {
    			myuser.setFirstName(user.getFirstName());	
    			myuser.setMiddleName(user.getMiddleName());
    			myuser.setLastName(user.getLastName());
    			myuser.setUserName(user.getUserName());
    			myuser.setPassWord(user.getPassWord());
    			myuser.setEmail(user.getEmail());
    			myuser.setAddress(user.getAddress());
    			myuser.setPhoneNumber(user.getPhoneNumber());
    			myuser.setHeight(user.getHeight());
    			myuser.setWeight(user.getWeight());
    	}
    		return userService.updateUserDetails(myuser);
    }

}
