package com.userfrom.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.userfrom.model.User;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

	User findById(int id);

	User deleteById(int id);
	
	List<User> findByUserName(String userName);

	Object findByUsername(String username);

}
