package com.userfrom.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.userfrom.model.User;
import com.userfrom.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public User save(User user) {
		return userRepository.save(user);
	}

	public User getUserDetails(int id) {
		return userRepository.findById(id);
	}

	public User deleteUserDetails(int id) {
		return userRepository.deleteById(id);
	}

	public User updateUserDetails(User myuser) {
		return userRepository.save(myuser);
	}
}
