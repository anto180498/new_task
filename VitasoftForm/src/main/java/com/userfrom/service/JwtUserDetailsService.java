package com.userfrom.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.userfrom.model.User;
import com.userfrom.repository.UserRepository;

@Service
public class JwtUserDetailsService {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder bcryptEncoder;
	
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		User user = userRepository.findByUserName(userName).get(0);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + userName);
		}
		return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassWord(),
				new ArrayList<>());

	}
	
	public User save(User user) {
		user.setUserName(user.getUserName());
		user.setPassWord(bcryptEncoder.encode(user.getPassWord()));
		return userRepository.save(user);
	}

}

